# Aula 03 - Recursão e busca

## Material

1. [Recursao](https://gitlab.com/ds143-alexkutzke/material/raw/main/03/00_recursao/recursion.pdf);
   - [Códigos](https://gitlab.com/ds143-alexkutzke/material/tree/main/03/00_recursao/codes);
2. [Busca](https://gitlab.com/ds143-alexkutzke/material/raw/main/03/01_busca/busca.pdf);
   - [Códigos](https://gitlab.com/ds143-alexkutzke/material/tree/main/03/01_busca/codes).

## Exercícios para praticar (não precisa entregar)
   
1) Implemente o algoritmo de busca sequencial e realize buscas em um vetor ordenado de inteiros com 1.000.000 elementos.

2) Implemente o algoritmo de busca binária (iterativo) e realize buscas em um vetor ordenado de inteiros com 1.000.000 elementos. Protótipo:

```c
int Busca (int V[], int n, int elem);
```

3) Implemente o algoritmo de busca binária (recursivo) e realize buscas em um vetor ordenado de inteiros com 1.000.000 elementos.

4) Qual a vantagem do algoritmo de busca sequencial sobre o algoritmo de busca binária?

5) Qual a vantagem do algoritmo de busca binária sobre o algoritmo de busca sequencial?

