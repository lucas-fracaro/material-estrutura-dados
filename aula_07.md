# Aula 07 - Árvores e Percursos

## Material

1. [Árvores](https://gitlab.com/ds143-alexkutzke/material/raw/main/07/00_arvores/arvores.pdf);
2. [Percursos e Propriedades de árvores](https://gitlab.com/ds143-alexkutzke/material/raw/main/07/01_percursos/percursos.pdf);
2. [Implementação simples de árvore](07/codes/tree.c);
